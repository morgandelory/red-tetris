const path = require('path');

module.exports = {
    mode: 'development',
    entry: ['./src/client/index.js'],
    devServer: {
        static: {
            directory: path.join(__dirname, 'public/'),
        },
        port: 3000,
    },
    output: {
        path: path.join(__dirname, 'public/'),
        filename: 'bundle.js'
    },
    module: {
        rules: [
            {
                test: /\.m?js$/,
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: [
                            ['@babel/preset-env', {targets: "defaults"}],
                            ['@babel/preset-react', {targets: "defaults"}]
                        ]
                    }
                }
            }
        ]
    }
};