import {matCollide, matDraw, matHighestCollide} from "./mat";

test('matCollide empty', () => {
    const m1 = [
        [0, 0, 0],
        [0, 0, 0],
        [0, 0, 0],
    ]
    const m2 = [
        [1, 0],
        [1, 1],
    ]
    expect(matCollide(m1, m2, 0, 0)).toBe(false);
})

test('matCollide complement', () => {
    const m1 = [
        [0, 1, 1],
        [0, 0, 1],
        [0, 0, 0],
    ]
    const m2 = [
        [0, 0],
        [1, 0],
        [1, 1],
    ]
    expect(matCollide(m1, m2, 1, 0)).toBe(false);
})

test('matCollide border collide', () => {
    const m1 = [
        [0, 0, 0],
        [0, 0, 0],
        [0, 0, 0],
    ]
    const m2 = [
        [1, 0],
        [1, 1],
    ]
    expect(matCollide(m1, m2, 2, 0)).toBe(true);
})

test('matCollide bottom collide', () => {
    const m1 = [
        [0, 0, 0],
        [0, 0, 0],
        [0, 0, 0],
    ]
    const m2 = [
        [1, 0],
        [1, 1],
    ]
    expect(matCollide(m1, m2, 0, 2)).toBe(true);
})

test('matDraw center', () => {
    const m1 = [
        [0, 0, 0],
        [0, 0, 0],
        [0, 0, 0],
    ]
    const m2 = [
        [1, 0],
        [1, 1],
    ]
    const expected = [
        [0, 0, 0],
        [0, 1, 0],
        [0, 1, 1],
    ]
    matDraw(m1, m2, 1, 1)
    expect(m1).toEqual(expected);
})

test('matDraw border', () => {
    const m1 = [
        [0, 0, 0],
        [0, 0, 0],
        [0, 0, 0],
    ]
    const m2 = [
        [1, 0],
        [1, 1],
    ]
    const expected = [
        [0, 0, 0],
        [0, 0, 1],
        [0, 0, 1],
    ]
    matDraw(m1, m2, 2, 1)
    expect(m1).toEqual(expected);
})

test('matHighestCollide y = 0', () => {
    const m1 = [
        [0, 0, 0, 0],
        [0, 0, 0, 0],
        [0, 1, 1, 0],
        [0, 0, 1, 0],
        [0, 0, 1, 0],
        [0, 0, 1, 0],
        [0, 1, 1, 0],
    ]
    const m2 = [
        [1, 1],
        [1, 0],
        [1, 0],
    ]
    expect(matHighestCollide(m1, m2, 0, 0)).toEqual(1);
})

test('matHighestCollide y != 0', () => {
    const m1 = [
        [0, 0, 0, 0],
        [0, 0, 0, 0],
        [0, 1, 1, 0],
        [0, 0, 1, 0],
        [0, 0, 1, 0],
        [0, 0, 1, 0],
        [0, 1, 1, 0],
    ]
    const m2 = [
        [1, 1],
        [1, 0],
        [1, 0],
    ]
    expect(matHighestCollide(m1, m2, 0, 4)).toEqual(4);
})