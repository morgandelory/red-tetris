export const matDraw = (m1, m2, x, y) => {
    for (let shapeY = 0; shapeY < m2.length; shapeY++) {
        for (let shapeX = 0; shapeX < m2[shapeY].length; shapeX++) {
            if (m2[shapeY][shapeX] !== 0 && y + shapeY < m1.length
                && y + shapeY >= 0
                && x + shapeX < m1[shapeY].length) {
                m1[y + shapeY][x + shapeX] = m2[shapeY][shapeX];
            }
        }
    }
}

export const matHighestCollide = (m1, m2, x, y) => {
    for (let i = y; i < m1.length; i++) {
        if(matCollide(m1, m2, x, i)) {
            return i - 1;
        }
    }
    return 0;
}

export const matCollide = (m1, m2, x, y) => {
    for (let shapeY = 0; shapeY < m2.length; shapeY++) {
        for (let shapeX = 0; shapeX < m2[shapeY].length; shapeX++) {
            if ((x + shapeX === m1[shapeY].length && m2[shapeY][shapeX] !== 0) ||
                (x < 0 && m2[shapeY][0] !== 0)) {
                return true;
            }
            if (m2[shapeY].every(t => t === 0) === false && y + shapeY >= 0) {
                if (y + shapeY === m1.length || (m1[y + shapeY][x + shapeX] !== 0 && m2[shapeY][shapeX] !== 0)) {
                    return true;
                }
            }
        }
    }
    return false;
}