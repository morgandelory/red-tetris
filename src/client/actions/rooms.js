import socket from "../socket";
import {
    CREATE_ROOM,
    JOIN_ROOM,
    LEAVE_ROOM,
    UPDATE_ROOM,
    UPDATE_ROOMS_LIST
} from "../../shared/constants/socketActions";

const fetchRoomsList = () => socket.emit(UPDATE_ROOMS_LIST);
const createRoom = params => socket.emit(CREATE_ROOM, params);
const joinRoom = roomId => socket.emit(JOIN_ROOM, roomId);
const leaveRoom = () => socket.emit(LEAVE_ROOM);
const fetchRoomInfo = () => socket.emit(UPDATE_ROOM);


export {
    fetchRoomsList,
    createRoom,
    joinRoom,
    leaveRoom,
    fetchRoomInfo
}