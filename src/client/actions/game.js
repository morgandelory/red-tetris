import {RESET_GAME, START_ROOM} from "../../shared/constants/socketActions";
import socket from "../socket";

const startGame = () => socket.emit(START_ROOM);
const resetGame = () => socket.emit(RESET_GAME);

export {
    startGame,
    resetGame
}
