import {AUTH} from "../../shared/constants/socketActions";
import socket from "../socket";

const login = name => socket.emit(AUTH, name);

export {
    login
}