import React from "react"

import {Route, Router, Switch} from "react-router-dom";
import Room from "./pages/Room/Room";
import Home from "./pages/Home/Home";
import Login from "./pages/Login/Login";
import history from "./history";
import {useSelector} from "react-redux";


function RedTetris() {
    const auth = useSelector(state => state.auth);

    return auth.id === null ? <Login/> :
        <Router history={history}>
            <Switch>
                <Route path={'/:roomId'} render={props => <Room {...props}/>}/>
                <Route><Home/></Route>
            </Switch>
        </Router>
}

export default RedTetris;
