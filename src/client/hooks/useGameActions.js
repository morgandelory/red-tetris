import {useDispatch} from "react-redux";
import socket from "../socket";
import {
    ACTION,
    ACTION_DOWN,
    ACTION_DROP,
    ACTION_LEFT,
    ACTION_RIGHT,
    ACTION_ROTATE, ACTION_SWAP, UPDATE_PLAYER
} from "../../shared/constants/socketActions";
import {SET_TARGET} from "../../shared/constants/game";

const useGameActions = () =>  {
    const dispatch = useDispatch();

    const left = () => socket.emit(ACTION, ACTION_LEFT);
    const right = () => socket.emit(ACTION, ACTION_RIGHT);
    const down = isPressingDown => socket.emit(ACTION, ACTION_DOWN, isPressingDown);
    const drop = () => socket.emit(ACTION, ACTION_DROP);
    const rotate = () => socket.emit(ACTION, ACTION_ROTATE);
    const swap = () => socket.emit(ACTION, ACTION_SWAP);
    const setTarget = target => socket.emit(SET_TARGET, target);
    const reset = () => dispatch({type: UPDATE_PLAYER, payload: {}});

    return {left, right, down, drop, rotate, swap, setTarget, reset};
}

export default useGameActions;