import useGameActions from "./useGameActions";
import useEventListener from "./useEventListener";
import {useState} from "react";
import lodash from "lodash";

const useGameController = () => {
    const actions = useGameActions();

    const [speedState, setSpeedState] = useState(false);

    useEventListener('keydown', lodash.throttle(event => {
        switch (event.key) {
            case 'ArrowLeft':
                actions.left();
                break;
            case 'ArrowRight':
                actions.right();
                break;
            case 'ArrowDown':
                if (speedState === false) {
                    actions.down(true);
                    setSpeedState(true);
                }
                break;
            case ' ':
                actions.drop();
                break;
            case 'ArrowUp':
                actions.rotate();
                break;
            case 'Shift':
                actions.swap();
                break;
            default:
                break;
        }
    }, 50));

    useEventListener('keyup', lodash.throttle(event => {
        switch (event.key) {
            case 'ArrowDown':
                actions.down(false);
                setSpeedState(false);
                break;
            default:
                break;
        }
    }, 50));
}

export default useGameController;