import React from "react";
import Container from "../Grid/Container";
import Item from "../Grid/Item";
import IText from "../IText/IText";


const ChoiceInput = props => {
    const {
        value = 0,
        choices,
        onChange = _ => _,
    } = props;

    const updateValue = newValue => {
        onChange(choices[Math.min(Math.max(newValue, 0), choices.length)][0]);
    }

    return <Container justifyContent={'center'} spacing={[0]} alignItems={'center'} style={{textAlign: 'center'}}>
        <Item><IText onClick={() => updateValue(value - 1)}>{'<-'}</IText></Item>
        <Item>
            {choices[value][1]}
        </Item>
        <Item><IText onClick={() => updateValue(value + 1)}>{'->'}</IText></Item>
    </Container>
}

export default ChoiceInput;