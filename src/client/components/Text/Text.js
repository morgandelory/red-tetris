import React from "react";
import styled from 'styled-components';

const Text = styled.span`
  color: ${({color}) => color};
  font-size: ${({size}) => size};
`

export default Text;