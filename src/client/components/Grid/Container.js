import React from "react";
import styled from 'styled-components';
import Item from "./Item";

const Container = props => {
    const {
        spacing = [1],
        gridSize = 12,
        direction = 'row',
        justifyContent = 'flex-start',
        alignContent = 'stretch',
        alignItems = 'center',
        noWrap = false,
        style,
        children,
    } = props;
    return <Wrapper
        spacingX={spacing[0]}
        spacingY={typeof spacing[1] === 'number' ? spacing[1] : spacing[0]}
        direction={direction}
        justifyContent={justifyContent}
        alignContent={alignContent}
        alignItems={alignItems}
        noWrap={noWrap}
        style={style}
    >
        {
            React.Children.toArray(children).map(
                (item, index) =>
                    item && <Item key={index}
                                  width={item.props.width}
                                  flexGrow={item.props.flexGrow}
                                  style={item.props.style}
                                  spacing={spacing}
                                  gridSize={gridSize}

                    >{item.props.children}</Item>
            )
        }
    </Wrapper>
}

const Wrapper = styled.div`
  margin: -${(props) => props.spacingX}px -${(props) => props.spacingY}px;
  width: 100%;
  display: flex;
  flex-direction: ${(props) => props.direction};
  justify-content: ${(props) => props.justifyContent};
  align-content: ${(props) => props.alignContent};
  align-items: ${(props) => props.alignItems};
  flex-wrap: ${(props) => props.noWrap === true ? 'nowrap' : 'wrap'};
  }
`;

export default Container;