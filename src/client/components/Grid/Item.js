import React from 'react';
import styled from 'styled-components';

export const Item = props => {
    const {
        gridSize,
        width = 'auto',
        spacing,
        children,
        flexGrow = 1,
        ...otherProps
    } = props;
    return <Wrapper
        width={width}
        spacingX={spacing[0]}
        spacingY={typeof spacing[1] === 'number' ? spacing[1] : spacing[0]}
        gridSize={gridSize}
        flexGrow={flexGrow}
        {...otherProps}
    >{children}</Wrapper>;
}
const Wrapper = styled.div`
  flex: ${(props) => props.flexGrow} 1 0;
  padding: ${(props) => props.spacingX * 8}px ${(props) => props.spacingY * 8}px;
  width: ${(props) => props.width !== 'auto' ? `${(props.width / props.gridSize) * 100}%` : null};
  flex: ${(props) => props.width !== 'auto' ? 'none !important' : null};
}
`;

export default Item;