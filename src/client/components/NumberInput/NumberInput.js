import React from "react";
import Container from "../Grid/Container";
import Item from "../Grid/Item";
import IText from "../IText/IText";


const NumberInput = props => {
    const {
        value = 0,
        step = 1,
        onChange,
        min,
        max
    } = props;

    const val = Number(value);

    const updateValue = newValue => onChange(Math.max(min ?? newValue, Math.min(newValue, max ?? newValue)));

    return <Container justifyContent={'center'} spacing={[0]} alignItems={'center'} style={{textAlign: 'center'}}>
        <Item><IText onClick={() => updateValue(val - step)}>-</IText></Item>
        <Item>
            {value}
        </Item>
        <Item><IText onClick={() => updateValue(val + step)}>+</IText></Item>
    </Container>
}

export default NumberInput;