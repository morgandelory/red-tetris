import React from "react";
import styled, {css} from "styled-components";

const Block = styled.div`
  margin: 0;
  width: min(${props => (props.ratio ?? 1) * 4}vh, ${props => (props.ratio ?? 1) * 2}vw);
  height: min(${props => (props.ratio ?? 1) * 4}vh, ${props => (props.ratio ?? 1) * 2}vw);
  background-color: ${props => props.background ? 'black' : null};
  ${({color}) => {
    const col = color === -1 ? 10 : color;

    return col !== 0 ? css`
      border-radius: 2px;
      background-image: url('blocks.jpg');
      opacity: ${props => props.opacity ?? 1};
      background-size: 405%;
      background-position: ${props => `${Math.trunc(col / 4) * 33}% ${(col % 4) * 33}%`};
    ` : null
  }
}
`

export default Block;