import React from "react";
import styled from 'styled-components';
import Text from "../Text/Text";

const IText = props => <Wrapper>
    <Text {...props}>{props.children}</Text>
</Wrapper>

const Wrapper = styled.div`
    &:hover {
      cursor: pointer;
      text-shadow: #FC0 1px 1px 5px;
    }
`

export default IText;