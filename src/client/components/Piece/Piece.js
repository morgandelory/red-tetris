import Container from "../Grid/Container";
import Item from "../Grid/Item";
import Block from "../Block/Block";
import React from "react";

const Piece = props => {
    const {
        ratio = 1,
        piece,
        background = false,
    } = props;

    return <Container direction={'column'} spacing={[0]} noWrap>
        {
            piece.map(row => <Item><Container spacing={[0]} noWrap>
                {
                    row.map(tile => <Item><Block background={background} ratio={ratio} color={tile}/></Item>)
                }
            </Container></Item>)
        }
    </Container>
}

export default Piece;