import Container from "../Grid/Container";
import Item from "../Grid/Item";
import Block from "../Block/Block";
import React from "react";
import PIECES from "../../../shared/constants/pieces";
import Piece from "../Piece/Piece";

const NormalizedPiece = props => {
    const {
        ratio = 1,
        piece,
    } = props;

    const mat = piece.length !== piece[0].length ? [[0, 0, 0, 0], ...piece] : piece;
    const rat = ratio * (3 / mat.length);

    return <Piece background={true} piece={mat} ratio={rat}/>
}

export default NormalizedPiece;