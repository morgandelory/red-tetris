import styled from "styled-components";

const Card = styled.div`
  color: white;
  border-radius: 8px;
  background-color: rgba(0, 0, 0, 0.6);
`

export default Card;