import React from "react";
import styled from "styled-components";

const LineInput = styled.input`
  width: 100%;
  padding: 0;
  color: palevioletred;
  border: none;
  border-radius: 3px;
`;

export default LineInput;