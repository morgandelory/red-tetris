import styled from "styled-components"

const Background = styled.div`
  background: url('bg.jpg') center;
  background-size: 120%;
  width: 100vw;
  height: 100vh;
  @keyframes brightness {
    from {
      box-shadow: inset 0 0 0 150vw rgba(141, 55, 122, 0);
    }
    to {
      box-shadow: inset 0 0 0 150vw rgba(141, 55, 122, 0.25);
    }
  }
  animation: 1s ease-in-out infinite alternate brightness;
`

export default Background;