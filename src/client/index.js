import React from 'react';
import ReactDOM from 'react-dom';
import {Provider} from "react-redux";
import RedTetris from "./RedTetris";
import {store} from "./store";
import {setUpListeners} from "./socket";
import Background from "./Background";

setUpListeners(store.dispatch);

ReactDOM.render(
    <Provider store={store}>
        <style>{`
        @font-face {
            font-family: "Tetris";
            src: url("./PlayPretend.otf");
        }
        * {
            color: white;
            user-select: none;
            font-family: Tetris;
        }
        body {
            margin: 0;
            padding: 0;
        }
        `}</style>
        <React.StrictMode>
            <Background>
                <RedTetris/>
            </Background>
        </React.StrictMode>
    </Provider>,
    document.getElementById('root')
);
