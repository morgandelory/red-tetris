import React, {useState} from "react";
import Item from "../../components/Grid/Item";
import Container from "../../components/Grid/Container";
import LineInput from "../../components/LineInput/LineInput";
import Card from "../../components/Card/Card";
import {login} from "../../actions/auth";
import IText from "../../components/IText/IText";
import {useSelector} from "react-redux";
import Text from "../../components/Text/Text";

const Login = props => {
    const [name, setName] = useState('');
    const error = useSelector(state => state.auth.error);

    const submit = () => login(name);

    return <Container justifyContent={'center'} alignItems={'center'} style={{width: '100vw', height: '100vh'}}>
        <Item width={3}>
            <Card>
                <Container direction={'column'} justifyContent={'center'} alignItems={'center'}>
                    <Item>What's your name ?</Item>
                    <Item>
                        <LineInput value={name} onChange={event => setName(event.target.value)}/>
                    </Item>
                    <Item>
                        <Text color={'red'} size={'12px'}>{error}</Text>
                    </Item>
                    <Item flexGrow={0}><IText onClick={submit}>Play</IText></Item>
                </Container>
            </Card>
        </Item>
    </Container>
}

export default Login;