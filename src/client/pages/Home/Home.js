import React from "react"
import RoomsList from "./RoomsList/RoomsList";
import RoomForm from "./RoomForm/RoomForm";
import Container from "../../components/Grid/Container";
import Item from "../../components/Grid/Item";
import Card from "../../components/Card/Card";

const Home = () => {
    return <Container direction={'column'} spacing={[1, 0]}>
        <Item><h1>Red Tetris</h1></Item>
        <Item width={12}>
            <Container justifyContent={'space-evenly'} alignItems={'stretch'}>
                <Item width={8}>
                    <Card style={{height: '85vh'}}><RoomsList/></Card>
                </Item>
                <Item>
                    <Card style={{height: '85vh'}}><RoomForm/></Card>
                </Item>
            </Container>
        </Item>
    </Container>
}

export default Home;