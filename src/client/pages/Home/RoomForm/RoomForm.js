import React, {useState} from "react"
import Container from "../../../components/Grid/Container";
import Item from "../../../components/Grid/Item";
import LineInput from "../../../components/LineInput/LineInput";
import NumberInput from "../../../components/NumberInput/NumberInput";
import {createRoom} from "../../../actions/rooms";
import ChoiceInput from "../../../components/ChoiceInput/ChoiceInput";
import {
    CFG_PLAYER_VIEW_NONE,
    CFG_PLAYER_VIEW_NORMAL,
    CFG_PLAYER_VIEW_SPECTERS
} from "../../../../shared/constants/game";
import IText from "../../../components/IText/IText";

const RoomForm = () => {
    const [params, setParams] = useState({
        name: '',
        password: '',
        gameSpeed: 1.0,
        maxUsers: 16,
        playersView: CFG_PLAYER_VIEW_NORMAL,
    });

    const submit = () => createRoom(params);
    const updateParams = newParams => setParams({...params, ...newParams});

    return <Container alignItems={'center'} justifyContent={'space-evenly'}>
        <Item width={11}>
            <Container style={{borderBottom: 'solid white 2px'}}><Item width={8}>Create room</Item></Container>
        </Item>
        <Item width={5}>Name:</Item>
        <Item width={5}>
            <LineInput value={params.name}
                       onChange={({target}) => updateParams({name: target.value})}/>
        </Item>
        <Item width={5}>Password:</Item>
        <Item width={5}>
            <LineInput value={params.password}
                       onChange={({target}) => updateParams({password: target.value})}/>
        </Item>
        <Item width={5}>Max Players:</Item>
        <Item width={5}>
            <NumberInput value={params.maxUsers}
                         onChange={value => updateParams({maxUsers: value})} step={1} min={1}
                         max={16}/>
        </Item>
        <Item width={5}>Game speed:</Item>
        <Item width={5}>
            <NumberInput value={params.gameSpeed}
                         onChange={value => updateParams({gameSpeed: value.toFixed(1)})}
                         step={0.1} min={0.5} max={5.0}/>
        </Item>
        <Item width={5}>Players View:</Item>
        <Item width={5}>
            <ChoiceInput value={params.playersView} choices={[
                [CFG_PLAYER_VIEW_NORMAL, 'Normal'],
                [CFG_PLAYER_VIEW_SPECTERS, 'Specters'],
                [CFG_PLAYER_VIEW_NONE, 'None'],
            ]} onChange={value => updateParams({playersView: value})}/>
        </Item>
        <Item flexGrow={0}>
            <IText onClick={submit}>Create</IText>
        </Item>
    </Container>
}

export default RoomForm;