import React, {useEffect} from "react";
import Container from "../../../components/Grid/Container";
import Item from "../../../components/Grid/Item";
import {Link} from "react-router-dom";
import {useSelector} from "react-redux";
import {fetchRoomsList} from "../../../actions/rooms";
import {RiRefreshLine} from "react-icons/ri";
import styled from "styled-components";

const RefreshButton = styled(RiRefreshLine)`
  @keyframes rotation {
    from {
      transform: rotate(0deg);
    }
    to {
      transform: rotate(360deg);
    }
  }
  cursor: pointer;
  font-size: 24px;
  &:hover {
    animation: 1s infinite linear rotation;
  }
`;


const RoomEntry = props => {
    return <Link to={'/' + props.id} style={{textDecoration: 'none'}}>
        <Container>
            <Item width={5}>{props.name}</Item>
            <Item width={4}>{props.host.name}</Item>
            <Item>{props.playersCount}/{props.maxPlayers}</Item>
        </Container>
    </Link>;
}

const RoomsList = () => {
    const roomsList = useSelector(state => state.rooms);

    useEffect(fetchRoomsList, []);

    return <Container direction={'column'} alignItems={'stretch'}>
        <Item>
            <Container style={{borderBottom: 'solid white 2px'}}>
                <Item width={5}>Name</Item>
                <Item width={4}>Host</Item>
                <Item>Players</Item>
                <Item flexGrow={0}><RefreshButton onClick={() => fetchRoomsList()}/></Item>
            </Container>
            {
                roomsList.map(r => <RoomEntry key={r.id} {...r}/>)
            }
        </Item>
    </Container>
}

export default RoomsList;