import React from "react";
import Container from "../../../../components/Grid/Container";
import {useSelector} from "react-redux";
import BufferBlock from "./BufferBlock";
import {isEqual} from "lodash";

const Buffer = () => {
    const buffer = useSelector(state => state.player.buffer, isEqual);
    const time = Date.now();

    return <Container direction={'column'} spacing={[0]}>
        {
            buffer?.map(block => {
                return <item><BufferBlock time={time - block}/></item>
            })
        }
    </Container>
}

export default Buffer;