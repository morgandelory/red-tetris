import React from "react";
import styled, {css} from "styled-components";

const BufferBlock = styled.div`
  margin: 0;
  width: min(4vh, 2vw);
  height: min(4vh, 2vw);
  border-radius: 2px;
  background-size: 405%;
  background-image: url('blocks.jpg');
  background-position: 0 66%;
  ${({time}) => time > 3000 ?
    css`
            @keyframes blink {
              50% {
                opacity: 0.0;
              }
            }
            animation: blink 0.2s step-start 0s infinite;
          ` : null
}
}
`

export default BufferBlock;