import React from "react";
import Container from "../../../components/Grid/Container";
import Item from "../../../components/Grid/Item";
import Board from "./Board/Board";
import {useSelector} from "react-redux";
import NextPieces from "./NextPieces/NextPieces";
import Buffer from "./Buffer/Buffer";
import {isEqual} from "lodash";
import Swap from "./Swap/Swap";
import {startGame} from "../../../actions/game";
import IText from "../../../components/IText/IText";
import {STATES} from "../../../../shared/constants/const";
import Results from "../Results/Results";
import Piece from "../../../components/Piece/Piece";
import {matHighestCollide} from "../../../../shared/helpers/mat";
import useGameController from "../../../hooks/useGameController";

const ShadeWrapper = () => {
    const board = useSelector(state => state.player.board, isEqual);
    const piece = useSelector(state => state.player.piece, isEqual);

    const shadeY = piece ? matHighestCollide(board, piece.shape, piece.x, piece.y) : null;

    return piece ? <div style={{
        margin: '0',
        padding: '0',
        position: 'absolute',
        opacity: '0.4',
        top: `calc(${shadeY} * min(4vh, 2vw))`,
        left: `calc(${piece.x} * min(4vh, 2vw))`
    }}>
        <Piece ratio={1} piece={piece.shape}/>
    </div> : null
}

const PieceWrapper = () => {
    const piece = useSelector(state => state.player.piece, isEqual);

    return piece ? <div style={{
        margin: '0',
        padding: '0',
        position: 'absolute',
        top: `calc(${piece.y} * min(4vh, 2vw))`,
        left: `calc(${piece.x} * min(4vh, 2vw))`
    }}>
        <Piece ratio={1} piece={piece.shape}/>
    </div> : null
}

const BoardWrapper = props => {
    const board = useSelector(state => state.player.board, isEqual);

    return board ? <Board board={board}/> : null;
}

const StartButton = () => {
    const host = useSelector(state => state.room.host);
    const myId = useSelector(state => state.auth.id);
    const roomState = useSelector(state => state.room.state);

    if (roomState !== STATES.WAITING)
        return null;

    return <div
        style={{position: 'absolute', bottom: '5%', fontSize: '24px', width: '100%', textAlign: 'center'}}>
        {
            myId === host?.id ? <IText onClick={() => startGame()}>START GAME</IText> : 'WAITING FOR HOST'
        }
    </div>
}

const PlayArea = props => {
    useGameController();
    const results = useSelector(state => state.results);

    return <Container justifyContent={'space-evenly'} alignItems={'stretch'}>
        <Item>
            <Container direction={'column'} justifyContent={'space-between'} alignItems={'center'}
                       style={{height: '100%'}}>
                <Item flexGrow={0}><Swap/></Item>
                <Item flexGrow={0}><Buffer/></Item>
            </Container>
        </Item>
        <Item>
            <div style={{
                overflow: 'hidden',
                position: 'relative',
                backgroundColor: 'black',
                height: 'min(80vh, 40vw)',
                width: 'min(40vh, 20vw)',
                margin: 'auto'
            }}>
                {
                    results === null ? <>
                        <BoardWrapper/>
                        <PieceWrapper/>
                        <ShadeWrapper/>
                        <StartButton/>
                    </> : <Results {...results}/>
                }
            </div>
        </Item>
        <Item>
            <NextPieces/>
        </Item>
    </Container>;
}

export default PlayArea;