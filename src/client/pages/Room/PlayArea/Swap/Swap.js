import React from "react";
import {useSelector} from "react-redux";
import NormalizedPiece from "../../../../components/NormalizedPiece/NormalizedPiece";
import {isEqual} from "lodash";

const Swap = props => {
    const altPiece = useSelector(state => state.player.swap, isEqual);

    return altPiece ? <NormalizedPiece background normalize ratio={0.85} piece={altPiece}/> : null;
}

export default Swap;