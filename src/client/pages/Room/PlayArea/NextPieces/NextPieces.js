import React from "react";
import Container from "../../../../components/Grid/Container";
import Item from "../../../../components/Grid/Item";
import PIECES from "../../../../../shared/constants/pieces";
import {useSelector} from "react-redux";
import {isEqual} from "lodash"
import NormalizedPiece from "../../../../components/NormalizedPiece/NormalizedPiece";

const PieceWrapper = ({piece, ratio}) => <NormalizedPiece background normalize ratio={ratio}
                                                          piece={PIECES[piece].shapes[0]}/>;

const NextPieces = props => {
    const [next, ...others] = useSelector(state => state.player.nextPieces, isEqual) ?? [null, []];

    return next !== null ? <Container direction={'column'}>
        <Item><PieceWrapper ratio={0.85} piece={next}/></Item>
        {
            others.map(piece => <Item>
                <PieceWrapper ratio={0.6} piece={piece}/>
            </Item>)
        }
    </Container> : null;
}

export default NextPieces;