import React from "react";
import Container from "../../../../components/Grid/Container";
import Block from "../../../../components/Block/Block";
import Item from "../../../../components/Grid/Item";

const initBoard = new Array(20).fill(0).map(() => new Array(10).fill(0));

const Row = props => {
    return <Container spacing={[0]}>
        {
            props.row.map((tile, index) => <Item key={index}>
                <Block ratio={props.ratio} color={tile > 10 ? tile - 10 : tile} opacity={tile > 10 ? 0.3 : 1}/>
            </Item>)
        }
    </Container>
}

const Board = props => {
    const {
        board,
        ratio = 1.0,
    } = props;

    const boardToRender = board ?? initBoard;

    return <Container direction={'column'} gridSize={10} spacing={[0]}>
        {
            boardToRender.map((row, index) => <Item key={index}>
                <Row row={row} ratio={ratio}/>
            </Item>)
        }
    </Container>
}

export default Board;