import React from "react";
import {configure, shallow} from 'enzyme';
import Board from "./Board";
import Adapter from 'enzyme-adapter-react-16';
import Piece from "../../../../../server/Piece/Piece";

configure({adapter: new Adapter()});
describe('Board', () => {
    it('renders correctly', () => {
        const wrapper = shallow(<Board />);
        expect(wrapper).toMatchSnapshot();
    });
    it('renders with piece and shade', () => {
        const piece = new Piece(1).toJSON();

        const wrapper = shallow(<Board piece={piece}/>);
        expect(wrapper).toMatchSnapshot();
    })
});