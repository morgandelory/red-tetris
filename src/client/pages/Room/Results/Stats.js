import React from "react";
import Container from "../../../components/Grid/Container";
import Item from "../../../components/Grid/Item";

const Stats = ({stats}) => {
    return <Container justifyContent={'space-evenly'}>
        <Item width={7}>Rows completed:</Item><Item width={3}>{stats.rowsCompleted}</Item>
        <Item width={7}>Damage dealt:</Item><Item width={3}>{stats.damageDealt}</Item>
        <Item width={7}>Damage absorbed:</Item><Item width={3}>{stats.damageAbsorbed}</Item>
        <Item width={7}>Kills:</Item><Item width={3}>{stats.kills}</Item>
    </Container>
}

export default Stats;