import React from "react";
import Container from "../../../components/Grid/Container";
import Item from "../../../components/Grid/Item";
import Rank from "./Rank";
import Stats from "./Stats";
import IText from "../../../components/IText/IText";
import {useHistory} from "react-router-dom";
import {useSelector} from "react-redux";
import {STATES} from "../../../../shared/constants/const";
import {resetGame} from "../../../actions/game";

const Results = (props) => {
    const {
        rank = 1,
        ...stats
    } = props;

    const host = useSelector(state => state.room.host);
    const myId = useSelector(state => state.auth.id);
    const roomState = useSelector(state => state.room.state);

    const history = useHistory();

    return <Container direction={'column'} justifyContent={'space-evenly'} style={{height: 'min(80vh, 40vw)'}}>
        <Item flexGrow={0}>
            <Rank rank={rank}/>
        </Item>
        <Item flexGrow={0}>
            <Stats stats={stats}/>
        </Item>
        <Item flexGrow={0}>
            <Container>
                <Item><IText onClick={() => history.push('/')}>Leave</IText></Item>
                {
                    host?.id === myId && roomState === STATES.FINISHED &&
                    <Item><IText onClick={() => resetGame()}>Restart</IText></Item>
                }
            </Container>
        </Item>
    </Container>
}

export default Results;