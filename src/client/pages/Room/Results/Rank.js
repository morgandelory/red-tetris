import React from "react";

const Rank = ({rank}) => {
    return <div style={{fontSize: '72px'}}>{rank}</div>
}

export default Rank;