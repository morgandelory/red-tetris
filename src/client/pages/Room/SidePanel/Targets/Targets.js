import React from "react";
import Container from "../../../../components/Grid/Container";
import Item from "../../../../components/Grid/Item";
import PlayerPortrait from "./PlayerPortrait";

const Targets = props => {
    return <Container justifyContent={'space-around'}>
        {
            props.targets.map(target => <Item width={2}>
                    <PlayerPortrait key={target.id} target={target}/>
                </Item>
            )
        }
    </Container>
}

export default Targets;