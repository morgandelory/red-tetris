import React from "react";
import Container from "../../../../components/Grid/Container";
import Item from "../../../../components/Grid/Item";
import Board from "../../PlayArea/Board/Board";
import styled from "styled-components";
import {useSelector} from "react-redux";
import _ from "lodash"
import useGameActions from "../../../../hooks/useGameActions";

const PlayerName = styled.div`
  word-break: break-all;
  text-align: center;
  color: white;
  background-color: black;
`

const PlayerPortrait = props => {
    const {setTarget} = useGameActions();

    const target = useSelector(state => state.player.target);
    const player = useSelector(state => state.players.find(p => p.id === props.id), _.deepStrictEqual);

    const getStyle = () => target === props.id ? {border: 'solid red 3px'} : {};

    return <Container spacing={[0]} style={{cursor: 'pointer', backgroundColor: 'black', ...getStyle()}}
                      onClick={() => setTarget(props.id)}>
        <Item width={12}>
            <Board ratio={0.2} board={player.board}/>
        </Item>
        <Item width={12}>
            <PlayerName>{player.name}</PlayerName>
        </Item>
    </Container>

}

export default PlayerPortrait;