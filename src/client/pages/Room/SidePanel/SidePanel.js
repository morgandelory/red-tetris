import React from "react";
import Container from "../../../components/Grid/Container";
import PlayerPortrait from "./Targets/PlayerPortrait";
import Item from "../../../components/Grid/Item";

const SidePanel = (props) => {
    return <Container>
        {
            props.ids.map(p => <Item><PlayerPortrait player={p.id}/></Item>)
        }
    </Container>
};

export default SidePanel;