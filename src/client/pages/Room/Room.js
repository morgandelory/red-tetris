import React, {useEffect} from "react";
import Container from "../../components/Grid/Container";
import Item from "../../components/Grid/Item";
import Card from "../../components/Card/Card";
import PlayArea from "./PlayArea/PlayArea";
import PlayerPortrait from "./SidePanel/Targets/PlayerPortrait";
import {joinRoom, leaveRoom} from "../../actions/rooms";
import {shallowEqual, useSelector} from "react-redux";
import useGameActions from "../../hooks/useGameActions";


const Room = props => {
    const id = useSelector(state => state.auth.id);
    const players = useSelector(state => state.players.filter(p => p.id !== id).map(p => p.id), shallowEqual);

    const {reset} = useGameActions();

    useEffect(() => {
        joinRoom(props.match.params.roomId);
        return () => {
            reset();
            leaveRoom();
        }
    }, []);

    const half = Math.trunc(players.length / 2);

    return <Container justifyContent={'space-evenly'} alignItems={'center'} style={{width: '100vw', height: '100vh'}} spacing={[0]}>
        <Item>
            <Container spacing={[1]} justifyContent={'center'}>
                {
                    players.slice(0, half).map(pid => <Item key={pid} width={2}>
                        <PlayerPortrait id={pid}/>
                    </Item>)
                }
            </Container>
        </Item>
        <Item width={4.5}>
            <Card>
                <PlayArea/>
            </Card>
        </Item>
        <Item>
            <Container spacing={[1]} justifyContent={'center'}>
                {
                    players.slice(half, players.length).map(pid => <Item key={pid} width={2}>
                        <PlayerPortrait id={pid}/>
                    </Item>)
                }
            </Container>
        </Item>
    </Container>
}

export default Room;