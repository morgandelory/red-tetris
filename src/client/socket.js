import SocketClient from "socket.io-client";
import {
    AUTH_ERROR,
    AUTH_SUCCESS,
    CREATE_ROOM, GAME_RESULT,
    JOIN_ROOM_ERROR,
    UPDATE_GAME_INFO,
    UPDATE_PLAYER,
    UPDATE_ROOM,
    UPDATE_ROOMS_LIST
} from "../shared/constants/socketActions";

import history from './history'
import {API} from "../shared/constants/params";

const socket = new SocketClient(API, {transports: ['websocket']});

export const setUpListeners = dispatch => {
    socket.on(AUTH_SUCCESS, auth => dispatch({type: AUTH_SUCCESS, payload: auth}));
    socket.on(AUTH_ERROR, auth => dispatch({type: AUTH_ERROR, payload: auth}));
    socket.on(UPDATE_ROOMS_LIST, rooms => dispatch({type: UPDATE_ROOMS_LIST, payload: rooms}));
    socket.on(CREATE_ROOM, roomId => history.push('/' + roomId));
    socket.on(JOIN_ROOM_ERROR, () => history.push('/'));
    socket.on(UPDATE_ROOM, room => dispatch({type: UPDATE_ROOM, payload: room}));
    socket.on(UPDATE_GAME_INFO, info => dispatch({type: UPDATE_GAME_INFO, payload: info}));
    socket.on(UPDATE_PLAYER, player => dispatch({type: UPDATE_PLAYER, payload: player}));
    socket.on(GAME_RESULT, results => dispatch({type: GAME_RESULT, payload: results}));
 }

export default socket;