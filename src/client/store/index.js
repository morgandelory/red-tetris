import {applyMiddleware, createStore} from "redux";
import thunk from "redux-thunk";
import {reducers} from "./reducers";

const initState = {
    auth: {
        id: null,
        name: null,
        error: null
    },
    rooms: [],
    player: {},
    players: []
}

const store = createStore(
    reducers,
    initState,
    applyMiddleware(thunk)
);

export {
    store
};