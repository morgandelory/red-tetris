import {UPDATE_ROOMS_LIST} from "../../../shared/constants/socketActions";

const reducer = (state = [], action) => {
    switch (action.type) {
        case UPDATE_ROOMS_LIST:
            return action.payload;
        default:
            return state;
    }
};

export default reducer;