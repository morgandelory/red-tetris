import {combineReducers} from "redux";
import playerReducer from "./playerReducer";
import playersListReducer from "./playersListReducer";
import authReducer from "./authReducer";
import roomsReducer from "./roomsReducer";
import roomReducer from "./roomReducer";
import resultsReducer from "./resultsReducer";

const reducers = combineReducers({
    auth: authReducer,
    results: resultsReducer,
    room: roomReducer,
    rooms: roomsReducer,
    player: playerReducer,
    players: playersListReducer
});

export {reducers};