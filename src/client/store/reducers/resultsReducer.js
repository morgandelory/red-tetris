import {GAME_RESULT, UPDATE_ROOM} from "../../../shared/constants/socketActions";
import {STATES} from "../../../shared/constants/const";

const reducer = (state = null, action) => {
    switch (action.type) {
        case UPDATE_ROOM:
            if (action.payload.state === STATES.WAITING) {
                return null;
            }
            return state;
        case GAME_RESULT:
            return action.payload;
        default:
            return state;
    }
};

export default reducer;