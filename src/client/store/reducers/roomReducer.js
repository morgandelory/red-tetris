import {UPDATE_ROOM} from "../../../shared/constants/socketActions";

const initPlayers = [];

const reducer = (state = initPlayers, action) => {
    switch (action.type) {
        case UPDATE_ROOM :
            return action.payload;
        default:
            return state;
    }
};

export default reducer;