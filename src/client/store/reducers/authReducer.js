import {AUTH_ERROR, AUTH_SUCCESS} from "../../../shared/constants/socketActions";

const reducer = (state = null, action) => {
    switch (action.type) {
        case AUTH_ERROR:
        case AUTH_SUCCESS:
            return {...state, ...action.payload};
        default:
            return state;
    }
};

export default reducer;