import {UPDATE_PLAYER, UPDATE_ROOM} from "../../../shared/constants/socketActions";
import {STATES} from "../../../shared/constants/const";

const initPlayer = {}

const reducer = (state = initPlayer, action) => {
    console.log(action);
    switch (action.type) {
        case UPDATE_ROOM:
            if (action.payload.state === STATES.WAITING) {
                return initPlayer;
            }
            return state;
        case UPDATE_PLAYER:
            return action.payload;
        default:
            return state;
    }
};

export default reducer;