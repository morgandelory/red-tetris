import {UPDATE_GAME_INFO, UPDATE_ROOM} from "../../../shared/constants/socketActions";

const initPlayers = [];

const reducer = (state = initPlayers, action) => {
    switch (action.type) {
        case UPDATE_GAME_INFO:
            return action.payload.players;
        case UPDATE_ROOM:
            return action.payload.players;
        default:
            return state;
    }
};

export default reducer;