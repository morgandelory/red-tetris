import {
    ACTION,
    CREATE_ROOM,
    DISCONNECT,
    JOIN_ROOM,
    JOIN_ROOM_ERROR,
    LEAVE_ROOM, RESET_GAME,
    START_ROOM,
    UPDATE_GAME_INFO,
    UPDATE_ROOM,
    UPDATE_ROOMS_LIST,
    UPDATE_TTRIS_INFO
} from "../shared/constants/socketActions";
import {v4 as uuidv4} from 'uuid';

import TTris from "./TTris";
import {STATES} from "../shared/constants/const";
import {SET_TARGET, SET_TARGET_MODE} from "../shared/constants/game";

class User {
    constructor(name, socket) {
        this.id = uuidv4();
        this.socket = socket;
        this.name = name;
        this.room = null;

        this.socket.on(DISCONNECT, () => this.leaveRoom());
        this.socket.on(CREATE_ROOM, params => {
            const room = TTris.createRoom(params, this);
            this.socket.emit(CREATE_ROOM, room.id);
        });
        this.socket.on(JOIN_ROOM, roomId => this.joinRoom(TTris.getRoomById(roomId)));
        this.socket.on(LEAVE_ROOM, () => this.leaveRoom());
        this.socket.on(UPDATE_ROOMS_LIST, () => this.socket.emit(UPDATE_ROOMS_LIST, TTris.rooms));
        this.socket.on(UPDATE_TTRIS_INFO, () => this.socket.emit(UPDATE_TTRIS_INFO, TTris));
        this.socket.on(START_ROOM, () => this.room.start(this));
        this.socket.on(RESET_GAME, () => this.room.resetGame(this));
    }

    joinRoom(room) {
        if (room !== undefined && room.state === STATES.WAITING) {
            if (this.room !== null) {
                this.leaveRoom();
            }
            room.addUser(this);
            this.room = room;
            this.socket.on(UPDATE_ROOM, () => this.room.update());
        } else {
            this.socket.emit(JOIN_ROOM_ERROR);
        }
    }

    leaveRoom() {
        if (this.room !== null) {
            this.room.removeUser(this);
            this.room = null;
        }
        this.socket.removeAllListeners(ACTION);
        this.socket.removeAllListeners(UPDATE_ROOM);
        this.socket.removeAllListeners(UPDATE_GAME_INFO);
        this.socket.removeAllListeners(SET_TARGET);
        this.socket.removeAllListeners(SET_TARGET_MODE);
    }

    toJSON() {
        return {
            name: this.name,
            id: this.id,
        }
    }
}

export default User;