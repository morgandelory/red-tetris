import express from "express"
import {createServer} from "http"
import path from "path";
import TTris from "./TTris";

const app = express();
app.use(express.static('public'));
app.get("*", (req, res) =>
    res.sendFile(path.join(__dirname), 'index.html'));

const server = new createServer(app);
TTris.serve(server);