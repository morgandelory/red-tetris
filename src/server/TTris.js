import Room from "./Room";
import {AUTH, AUTH_ERROR, AUTH_SUCCESS, CONNECT} from "../shared/constants/socketActions";
import {Server} from "socket.io";
import User from "./User";
import {PORT} from "../shared/constants/params";
import {STATES} from "../shared/constants/const";

class TTris {
    constructor() {
        this._port = PORT;
        this._socket = null;

        this.users = [];
        this.rooms = [];
    }

    get socket() {
        return this._socket;
    }

    _setupSocket() {
        this._socket.on(CONNECT, socket => {
            socket.on(AUTH, (name) => {
                if (name === '') {
                    socket.emit(AUTH_ERROR, {error: 'Invalid username'});
                    return;
                }
                const user = new User(name, socket)
                this.users.push(user);

                socket.emit(AUTH_SUCCESS, user);

                if (process.env.NODE_ENV !== 'production') {
                    console.log('New player connected: ' + name);
                }
            })
        })
    }

    serve(server) {
        this._socket = new Server(server);
        this._setupSocket();

        server.listen(this._port, () => console.log(`Listening on port ${this._port}`));
    }

    getRoomById(roomId) {
        return this.rooms.find(room => room.id === roomId);
    }

    createRoom(params, host) {
        const newRoom = new Room(params, host);
        this.rooms.push(newRoom);

        if (process.env.NODE_ENV !== 'production') {
            console.log(host.name + ' created room ' + newRoom.id);
        }

        return newRoom;
    }

    deleteRoom(room) {
        if (room.game !== null) {
            if (process.env.NODE_ENV !== 'production') {
                console.log('Deleted game ' + room.game.id);
            }
            clearInterval(room.game.clock);
        }
        this.rooms = this.rooms.filter(r => r !== room);

        if (process.env.NODE_ENV !== 'production') {
            console.log('Deleted room ' + room.id);
        }
    }

    toJSON() {
        return {
            rooms: this.rooms.filter(r => r.state === STATES.WAITING),
        }
    }
}

export default new TTris();