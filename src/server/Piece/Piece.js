import PIECES from "../../shared/constants/pieces";

const getYOffset = shape => {
    let i = 0;

    while (shape[i].every(tile => tile === 0)) {
        i += 1;
    }
    return i;
}

class Piece {
    constructor(num) {
        this._shapes = PIECES[num].shapes;
        this.x = 5 - Math.trunc(PIECES[num].shapes[0].length / 2);
        this.y = 0 - getYOffset(this._shapes[0]);
        this._rotation = 0;
    }

    get nextShape() {
        return this._shapes[(this._rotation + 1) % 4];
    }

    get shape() {
        return this._shapes[this._rotation];
    }

    rotate() {
        this._rotation = (this._rotation + 1) % 4;
    }

    toJSON() {
        return {
            shape: this.shape,
            x: this.x,
            y: this.y
        }
    }
}

export default Piece;