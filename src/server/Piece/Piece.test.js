import Piece from "./Piece";

test('Piece rotate', () => {
    const piece = new Piece(0);

    piece.rotate();
    expect(piece._rotation).toBe(1);
});