class Buffer {
    constructor() {
        this.buffer = [];
    }

    attack(power) {
        const now = Date.now();
        for (let i = 0; i < power && this.buffer.length < 16; i++) {
            this.buffer.push(now);
        }
    }

    defend(power) {
        const overflow = power - this.buffer.length;
        this.buffer.splice(0, power);
        return overflow > 0 ? overflow : 0;
    }

    getTimedOutBlocks() {
        const now = Date.now();
        return this.buffer.filter(block => now - block >= 5000);
    }

    deleteBlocks(nBlocks) {
        this.buffer.splice(0, nBlocks);
    }

    toJSON() {
        return this.buffer;
    }
}

export default Buffer;