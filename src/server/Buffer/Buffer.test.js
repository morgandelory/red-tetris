import Buffer from "./Buffer";

test('Attack buffer', () => {
    const buffer = new Buffer();

    Date.now = jest.fn(() => 100000);
    buffer.attack(3);
    expect(buffer.buffer).toEqual([100000, 100000, 100000]);
});

test('Attack overflow', () => {
    const buffer = new Buffer();

    Date.now = jest.fn(() => 1);
    buffer.attack(16);
    expect(buffer.buffer).toEqual(Array(16).fill(1));
});

test('Defend buffer', () => {
    const buffer = new Buffer();

    buffer.buffer = [1, 2, 3, 4, 5];
    expect(buffer.defend(3)).toBe(0);
    expect(buffer.buffer).toEqual([4, 5]);
});

test('Defend overflow', () => {
    const buffer = new Buffer();

    buffer.buffer = [1, 2, 3, 4, 5];
    expect(buffer.defend(8)).toBe(3);
    expect(buffer.buffer).toEqual([]);
})

test('Get Timed Out Blocks', () => {
    const buffer = new Buffer();

    buffer.buffer = [1000, 2000, 3000, 4000, 5000];
    Date.now = jest.fn(() => 7000);
    expect(buffer.getTimedOutBlocks()).toEqual([1000, 2000]);
})

test('Delete Blocks' , () => {
    const buffer = new Buffer();

    buffer.buffer = [1000, 2000, 3000, 4000, 5000];
    buffer.deleteBlocks(2);
    expect(buffer.buffer).toEqual([3000, 4000, 5000]);
})

test('Delete Overflow' , () => {
    const buffer = new Buffer();

    buffer.buffer = [1000, 2000, 3000, 4000, 5000];
    buffer.deleteBlocks(10);
    expect(buffer.buffer).toEqual([]);
})

test('toJSON', () => {
    const buffer = new Buffer();

    buffer.buffer = [1000, 2000, 3000, 4000, 5000];
    expect(buffer.toJSON()).toEqual([1000, 2000, 3000, 4000, 5000]);
})