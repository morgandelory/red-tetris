import {matCollide, matDraw} from "../../shared/helpers/mat";

class Board {
    constructor() {
        this._data = new Array(20).fill(0).map(() => new Array(10).fill(0));
    }

    at(x, y) {
        return this._data[y][x];
    }

    isPieceValidAt(piece, x, y) {
        return matCollide(this._data, piece.shape, x, y) === false;
    }

    canPieceRotate(piece) {
        return matCollide(this._data, piece.nextShape, piece.x, piece.y) === false;
    }

    destroyRows(rows) {
        rows.forEach(index => {
            this._data.splice(index, 1);
            this._data.unshift(Array(10).fill(0))
        });
    }

    getCompletedRows() {
        return this._data.reduce((result, element, index) => {
            if (element.every(tile => tile !== 0 && tile !== -1)) {
                result.push(index);
            }
            return result;
        }, []);
    }

    insertBottomRows(nRows) {
        for (let i = 0; i < nRows; i++) {
            this._data.splice(0, 1);
            this._data.push(Array(10).fill(-1));
        }
    }

    drawPiece(piece) {
        matDraw(this._data, piece.shape, piece.x, piece.y);
    }

    toJSON() {
        return this._data;
    }
}

export default Board;