import Player from "./Player/Player";
import PIECES from "../shared/constants/pieces";
import TTris from "./TTris";
import {UPDATE_GAME_INFO} from "../shared/constants/socketActions";
import {getRandomInt} from "../shared/helpers/random";
import {STATES} from "../shared/constants/const";

class Game {
    constructor(room) {
        this.room = room;
        this.pieceStack = [];
        this.players = room.users.map(user => new Player(user.id, user.socket, this));
        this.alivePlayers = [...this.players];
        this.params = {...this.room.otherParams};

        this.clock = null;
        this.lastUpdate = 0;
        this.lastSpeedUpdate = Date.now();
    }

    get id() {
        return this.room.id;
    }

    genPiece() {
        this.pieceStack.push(getRandomInt(PIECES.length));
    }

    getPieces(i) {
        while (i + 5 > this.pieceStack.length) {
            this.genPiece();
        }
        return this.pieceStack.slice(i, i + 6);
    }

    tick() {
        const time = Date.now();
        const tickRef = 1000 / this.params.gameSpeed;

        this.alivePlayers = this.alivePlayers.filter(p => p.isAlive === true);
        this.alivePlayers.forEach(player => player.tick(time, tickRef));

        if (time - this.lastUpdate > 3000) {
            this.update();
            this.lastUpdate = time;
        }
        if (time - this.lastSpeedUpdate > 15000) {
            this.params.gameSpeed = Math.min(this.params.gameSpeed + 0.1, 5.0);
            this.lastSpeedUpdate = time;
        }
        if (this.isRunning === false) {
            clearInterval(this.clock);
            this.room.state = STATES.FINISHED;
            this.room.game = null;
            this.room.update();
        }
    }

    run() {
        this.clock = setInterval(() => this.tick(), 50);
    }

    update() {
        TTris.socket.to(this.id).emit(UPDATE_GAME_INFO, this);
    }

    get isRunning() {
        return this.alivePlayers.length > 0;
    }

    toJSON() {
        return {
            players: this.players
        }
    }
}

export default Game;