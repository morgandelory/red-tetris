import {v4 as uuidv4} from 'uuid';
import TTris from "./TTris";
import {UPDATE_ROOM} from "../shared/constants/socketActions";
import {STATES} from "../shared/constants/const";
import Game from "./Game";
import {getRandomInt} from "../shared/helpers/random";


class Room {
    constructor(params, host) {
        const {
            name,
            maxUsers,
            ...otherParams
        } = params;

        this.id = uuidv4();
        this.name = name;
        this.state = STATES.WAITING;
        this.host = host;
        this.maxUsers = maxUsers;
        this.users = [];

        this.game = null;

        this.otherParams = otherParams;
    }

    get usersCount() {
        return this.users.length;
    }

    addUser(user) {
        this.users.push(user);
        user.socket.join(this.id) ;
        this.update();
    }

    removeUser(user) {
        this.users = this.users.filter(p => p !== user);
        user.socket.leave(this.id);
        if (this.users.length === 0) {
            TTris.deleteRoom(this);
        } else {
            this.host = this.users[getRandomInt(this.users.length)];
        }
        this.update();
    }

    start() {
        if (this.state !== STATES.STARTED) {
            this.state = STATES.STARTED;
            this.update();
            this.game = new Game(this);
            this.game.run();
        }
    }

    resetGame() {
        if (this.state === STATES.FINISHED) {
            this.state = STATES.WAITING;
        }
        this.update();
    }

    update() {
        TTris.socket.to(this.id).emit(UPDATE_ROOM, this);
    }

    toJSON() {
        return {
            id: this.id,
            state: this.state,
            name: this.name,
            host: {id: this.host?.id, name: this.host?.name},
            playersCount: this.usersCount,
            maxPlayers: this.maxUsers,
            players: this.users,
        }
    }
}

export default Room;