import Board from "../Board/Board";
import Buffer from "../Buffer/Buffer";
import {
    ACTION,
    ACTION_DOWN,
    ACTION_DROP,
    ACTION_LEFT,
    ACTION_RIGHT,
    ACTION_ROTATE,
    ACTION_SWAP, GAME_RESULT,
    UPDATE_PLAYER, UPDATE_ROOM
} from "../../shared/constants/socketActions";
import {matHighestCollide} from "../../shared/helpers/mat";
import Piece from "../Piece/Piece";
import {
    SET_TARGET,
    SET_TARGET_MODE,
    TM_ATTACKERS,
    TM_RANDOM,
    TM_STRONGEST,
    TM_TARGET,
    TM_WEAKEST
} from "../../shared/constants/game";
import {getRandomInt} from "../../shared/helpers/random";

class Player {
    constructor(id, socket, game) {
        this._game = game;

        this.id = id;
        this.socket = socket;

        this.board = new Board();
        this.piece = null;
        this.altPiece = null;
        this.nextPieces = null;
        this.isPressingDown = false;

        this._piecesCount = 0;

        this.buffer = new Buffer();

        this._target = null;
        this._targetMode = TM_RANDOM;

        this.isAlive = true;

        this.lastTick = 0;

        this.stats = {
            rowsCompleted: 0,
            damageAbsorbed: 0,
            damageDealt: 0,
            kills: 0,
        }

        this._setupSocket();
        this.updatePieces();
    }

    _setupSocket() {
        this.socket.on(ACTION, (action, arg) => {
            switch (action) {
                case ACTION_RIGHT:
                    this.moveRight();
                    break;
                case ACTION_LEFT:
                    this.moveLeft();
                    break;
                case ACTION_DOWN:
                    this.isPressingDown = arg;
                    break;
                case ACTION_DROP:
                    this.drop();
                    break;
                case ACTION_ROTATE:
                    this.rotate();
                    break;
                case ACTION_SWAP:
                    this.swap();
                    break;
                default:
                    break;
            }
            this.update();
        });
        this.socket.on(SET_TARGET, target => {
            this._target = this._game.players.find(player => player.id === target);
            this._targetMode = TM_TARGET;
        });
        this.socket.on(SET_TARGET_MODE, mode => this._targetMode = mode);
    }

    moveLeft() {
        if (this.board.isPieceValidAt(this.piece, this.piece.x - 1, this.piece.y)) {
            this.piece.x -= 1;
        }
    }

    moveRight() {
        if (this.board.isPieceValidAt(this.piece, this.piece.x + 1, this.piece.y)) {
            this.piece.x += 1;
        }
    }

    moveDown() {
        if (this.board.isPieceValidAt(this.piece, this.piece.x, this.piece.y + 1)) {
            this.piece.y += 1;
        } else {
            this.validateCurrentPiece();
        }
    }

    rotate() {
        if (this.board.canPieceRotate(this.piece)) {
            this.piece.rotate();
        }
    }

    drop() {
        this.piece.y = matHighestCollide(this.board._data, this.piece.shape, this.piece.x, this.piece.y);
        this.validateCurrentPiece();
    }

    swap() {
        const tmp = this.altPiece;

        if (tmp === null) {
            this.altPiece = this.piece;
            this._piecesCount += 1;
            this.updatePieces();
        } else {
            if (this.board.isPieceValidAt(tmp, this.piece.x, this.piece.y)) {
                this.altPiece = this.piece;
                tmp.x = this.altPiece.x
                tmp.y = this.altPiece.y
                this.piece = tmp;
            }
        }
    }

    attack(power) {
        if (power > 0) {
            this.targets.forEach(target => {
                target.buffer.attack(power);
                target.update();
            });
        }
    }

    get targets() {
        switch (this._targetMode) {
            case TM_ATTACKERS:
                return [];
            case TM_WEAKEST:
                return [];
            case TM_STRONGEST:
                return [];
            case TM_TARGET:
                return [this._target]
            default:
                const targets = this._game.alivePlayers.filter(p => p !== this);
                return targets.length === 0 ? [] : [targets[getRandomInt(targets.length)]];
        }
    }

    validateCurrentPiece() {
        this.board.drawPiece(this.piece);
        this._piecesCount += 1;

        if (this.piece.y <= 0) {
            this.kill();
        } else {
            const completed = this.board.getCompletedRows();
            this.board.destroyRows(completed);
            this.stats.rowsCompleted += completed.length;

            const power = this.buffer.defend(completed.length);
            this.attack(power);
            this.stats.damageDealt += power;
            this.stats.damageAbsorbed += completed.length - power;

            this.updatePieces();
        }
    }

    kill() {
        this.stats.rank = this._game.alivePlayers.length;
        this.socket.emit(GAME_RESULT, this.stats);
        this.isAlive = false;

        this.socket.removeAllListeners(ACTION);
        this.socket.removeAllListeners(SET_TARGET);
        this.socket.removeAllListeners(SET_TARGET_MODE);
    }

    tick(currentTick, tickRef) {
        if (this.buffer.buffer.length > 0 && this.buffer.buffer[0] < currentTick) {
            const damages = this.buffer.getTimedOutBlocks();
            this.buffer.deleteBlocks(damages.length);
            this.board.insertBottomRows(damages.length);
        }
        if (currentTick > this.lastTick + (this.isPressingDown ? 50 : tickRef)) {
            this.moveDown();
            this.update();
            this.lastTick = currentTick;
        }
    }

    update() {
        this.socket.emit(UPDATE_PLAYER, this);
    }

    updatePieces() {
        const [next, ...others] = this._game.getPieces(this._piecesCount);

        this.piece = new Piece(next);
        this.nextPieces = others;
    }

    toJSON() {
        return {
            id: this.id,
            piece: this.piece,
            board: this.board,
            buffer: this.buffer,
            swap: this.altPiece?.shape,
            target: this._target?.id,
            targetMode: this._targetMode,
            nextPieces: this.nextPieces
        }
    }
}

export default Player;