import Player from "./Player";
import Piece from "../Piece/Piece";

describe('Player', () => {
    let player;

    beforeEach(() => {
        const game = {
            getPieces: () => [0, [1, 2, 3]]
        }
        const socket = {
            on: () => {},
            emit: () => {}
        }
        player = new Player('id', socket, game);
    })
    describe('swap', () => {

        test('with no swap piece', () => {
            const piece = player.piece;

            player.swap();

            expect(piece).toBe(player.altPiece);
            expect(player.piece).not.toBeNull();
        })

        test('with swap piece', () => {
            player.altPiece = new Piece(1);

            const piece = player.piece;
            const alt = player.altPiece;

            player.swap();

            expect(piece).toBe(player.altPiece);
            expect(alt).toBe(player.piece);
        })
    })
    describe('action', () => {
        describe('left', () => {

            test('success', () => {
                player.piece.x = 1;
                player.moveLeft();
                expect(player.piece.x).toBe(0);
            })

            test('fail', () => {
                player.piece.x = 0;
                player.moveLeft();
                expect(player.piece.x).toBe(0);
            })

        })

        describe('right', () => {

            test('success', () => {
                player.piece.x = 1;
                player.moveRight();
                expect(player.piece.x).toBe(2);
            })

            test('fail', () => {
                player.piece.x = 6;
                player.moveRight();
                expect(player.piece.x).toBe(6);
            })
        })

        describe('down', () => {

            test('success', () => {
                player.piece.y = 0;
                player.moveDown();
                expect(player.piece.y).toBe(1);
            })

            test('validate', () => {
                const mock = jest.spyOn(player, 'validateCurrentPiece')

                player.piece.y = 18;
                player.moveDown();
                expect(player.validateCurrentPiece).toBeCalled();
                mock.mockRestore();
            })
        })

        describe('rotate', () => {

            test('success', () => {
                player.piece.x = 5;
                player.rotate();
                expect(player.piece._rotation).toBe(1);
            })

            test('fail', () => {
                player.piece._rotation = 1;
                player.piece.x = -2;
                player.rotate();
                expect(player.piece._rotation).toBe(1);
            })
        })
    })
})